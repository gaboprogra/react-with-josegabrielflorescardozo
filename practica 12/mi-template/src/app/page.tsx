"use client"
import Header from "@/components/Header";
import * as React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import ImageCard from "@/components/ImageCard";
import { Button, Grid, Typography } from "@mui/material";
import Icon from "@/components/Icon";

export default function Home() {
  return (
    <main >
      <section className="conatainer-header">
        <Header></Header>
        <Container >
          <Box sx={{ bgcolor: 'transparent', height: '100vh' }}>
            <Box sx={{ bgcolor: 'transparent', height: '30vh' }}>
              <h1 className="text">Shaun <br />
                Murphy</h1>
            </Box>
            <Box sx={{ bgcolor: 'transparent', height: '30vh', display: "flex", flexDirection: "column" }}>
              <h2 className="texts">An aspiring UI/UX Designer. Who breathes life into pixels, crafting interfaces that not only engage but enchant.</h2>
            </Box>
            <Box sx={{ bgcolor: 'transparent', height: '10vh', marginLeft: 5 }}>
              <Button variant="outlined" disabled>
                Hire Me
              </Button>
              <Button variant="outlined" disabled>
                My Story
              </Button>
            </Box>
          </Box>
          <ImageCard imageUrl="./images/bg-hero.svg"></ImageCard>
        </Container>
      </section>
      <section>
        <Grid container xs={12}>
          <Grid item xs={4}>
            <Box component="div" border={2} height={500} p={5} >
              <Typography variant="h4" color="white" ml={9} mb={5}>
                About me
                <hr className="hr" />
              </Typography>
              <div className="container-img">
                <img src="./images/i.svg" alt="" className="image-i" />
              </div>
            </Box>
          </Grid>
          <Grid item xs={8}>
            <Box component="div" height={500} color="white" paddingTop={19} fontSize={30}>
              Hey there! I'm Shaun, a passionate UI/UX designer armed with creativity and a love for problem-solving. With a blend of design thinking and user-centric approach, I'm on a mission to create digital experiences that leave a lasting impression. So let's collaborate and bring your vision to life!
            </Box>
          </Grid>
        </Grid>
        <div className="container-icon">
          <Icon img="./images/icon.svg" text="Years of Design Experience"></Icon>
          <hr />
          <Icon img="./images/icon.svg" text="Years of Design Experience"></Icon>
          <hr />
          <Icon img="./images/icon.svg" text="Years of Design Experience"></Icon>
        </div>
      </section>
      <section>
        <Typography variant="h4" color="white" ml={9} mb={5}>
          Projects
          <hr className="hr" />
        </Typography>
        <Grid container xs={12}>
          <Grid item xs={6}>
            <Box component="div" border={2} display="flex" justifyContent="center">
              <img src="./images/pr1.svg" alt="" />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box component="div" border={2} display="flex" justifyContent="center">
              <img src="./images/pr2.svg" alt="" />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box component="div" border={2} display="flex" justifyContent="center">
              <img src="./images/pr3.svg" alt="" />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box component="div" border={2} display="flex" justifyContent="center">
              <img src="./images/pr4.svg" alt="" />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box component="div" border={2} display="flex" justifyContent="center">
              <img src="./images/pr5.svg" alt="" />
            </Box>
          </Grid>
        </Grid>
      </section>




    </main>
  );
}
