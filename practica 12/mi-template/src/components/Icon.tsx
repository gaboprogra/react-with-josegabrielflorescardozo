import * as React from 'react';
export default function Icon(props:{img:string, text:string}){
    return(
        <div className="icon">
            <img src={props.img} alt="" />
            <h2 className="ppp"> {props.text}</h2>
        </div>
    )

}