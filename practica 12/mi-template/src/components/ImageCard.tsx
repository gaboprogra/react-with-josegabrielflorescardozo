import React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
interface ImageCardProps {
    imageUrl: string;
  }
const ImageCard: React.FC<ImageCardProps> = ({ imageUrl }) => {
  return (
    <Card sx={{ maxWidth:'100%',backgroundColor:'transparent',display:'flex',alignItems:'center',justifyContent:'center'}}>
      <CardMedia
       component="img"
       sx={{ height: 600, width: 600, objectFit: 'cover' }}
       image={imageUrl}
       alt="Imagen"
      />
    </Card>
  );
};

export default ImageCard;