"use client"
import Image from "next/image";
import styles from "./page.module.css";
import Header from "@/components/Header";
import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import ImageCard from "@/components/ImageCard";
import { Button } from "@mui/material";

export default function Home() {
  return (
    <main >
      <Header></Header>
      <section>
      <Container >
        <Box sx={{ bgcolor: 'transparent', height: '100vh' }}>
          <Box sx={{ bgcolor: 'transparent', height: '30vh' }}>
          <h1 className="text">Shaun <br />
            Murphy</h1>
          </Box>
          <Box sx={{ bgcolor: 'transparent', height: '30vh', display:"flex",flexDirection:"column"}}>
            <h2 className="texts">An aspiring UI/UX Designer. Who breathes life into pixels, crafting interfaces that not only engage but enchant.</h2>

          </Box>
          <Box sx={{ bgcolor: 'transparent', height: '10vh',marginLeft:5}}>
          <Button variant="outlined" disabled>
            Hire Me
          </Button>
          <Button variant="outlined" disabled>
            My Story
          </Button>

          </Box>


        </Box>
        <ImageCard imageUrl="./images/bg-hero.svg"></ImageCard>
      </Container>

      </section>


    </main>
  );
}
